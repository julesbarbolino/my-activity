<?php 
	include('connection.php');
	$result = mysqli_query($conn, "SELECT * FROM user ORDER BY id DESC");
 ?>
<!DOCTYPE html>
<html>
<head>
	<title>Simple CRUD Program</title>
</head>
<body>
	<a href="add.html">Add New Data</a>
	<br/><br/>
	<table width='80%' border=0>
		<tr bgcolor="#CCCCCC">
			<td>Name</td>
			<td>Contact No.</td>
			<td></td>
		</tr>
	<?php 
		while ($res = mysqli_fetch_array($result)) {
			echo "<tr>";
			echo "<td>".$res['name']."</td>";
			echo "<td>".$res['contact']."</td>";
			echo "<td><a href=\"edit.php?id=$res[id]\">Edit</a> | <a href=\"delete.php?id=$res[id]\" onClick=\"return confirm('Are you sure you want to delete?')\">Delete</a></td>";
		}
	 ?>
	</table>
</body>
</html>